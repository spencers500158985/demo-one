// spencer Sauberlich
/*Demo 1*/

#include <iostream>
#include <conio.h>

using namespace std;

int main()
{
	std::cout << "Hello World!\n";

	int i;
	float f;
	double;
	bool b;
	char c;
	const double PI = 3.141926;
	//double const PI = 3.141926;

	cout << "Enter an integer:";
	cin >> i;

	if (i > 100) cout << "That's a big number.\n";
	else cout << "That's not a big number.\n";

	if (i) cout << "I is not zero.\n";
	
	// switch case ex online

	for (int i = 0; i <= 10; i++)
	{
		cout << i << "\n";
	}

	cout << "Do you want to do math? (y/n);";
	int x = 1;
	char input;
	cin >> input;

	while (input == 'y')
	{
		cout << "9 x " << x << " = " << 9 * x << "\n";
		cout << "again?";
		cin >> input;
		x++;
	}



	_getch();
	return 0;
}